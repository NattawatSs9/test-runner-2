import type { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  testDir: './tests',
  globalSetup: require.resolve('./global.setup.ts'),
  timeout: 0,
  expect: {
    timeout: 45000,
  },
  fullyParallel: false,
  forbidOnly: !!process.env.CI,
  retries: 1,
  workers: process.env.CI ? 1 : undefined,
  reporter: [
    [
      'junit',
      {
        outputFile: process.env.JUNIT_NUMBER
          ? `./result_log/results-${process.env.JUNIT_NUMBER}.xml`
          : './result_log/results.xml',
      },
    ],
    ['html'],
    [
      'blob',
      {
        outputDir: process.env.PWTEST_BLOB_REPORT_NAME
          ? `./blob-all-reports`
          : undefined,
      },
    ],
  ],
  use: {
    actionTimeout: 45 * 1000,
    navigationTimeout: 60 * 1000,
    baseURL: 'https://signac-app-dev.cjexpress.co.th/',
    headless: true,
    viewport: { width: 480, height: 800 },
    trace: 'retain-on-failure',
    locale: 'TH',
    timezoneId: 'Asia/Bangkok',
  },
  projects: [
    {
      name: 'Microsoft Edge',
      use: {
        channel: 'msedge',
      },
    },
  ],
};

export default config;
