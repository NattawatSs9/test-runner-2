import { test as base } from '@playwright/test';

//LineOA Pages
import { SigupPage } from './lineoa/pages/Signup/signup-page';
import { MemberPrivilegePage } from './lineoa/pages/MemberPrivilege/member-privilege-page';
import { OobePage } from './lineoa/pages/Oobe/oobe-page';
import { ContactUsPage } from './lineoa/pages/ContactUs/contact-us-page';
import { CouponsPrivilegeDetailPage } from './lineoa/pages/CouponsPrivilegeDetail/coupons-privilege-detail-page';
import { CouponsPrivilegePage } from './lineoa/pages/CouponsPrivilege/coupons-privilege-page';
import { DataConsentPage } from './lineoa/pages/DataConsent/data-consent-page';
import { EditProfilePage } from './lineoa/pages/EditProfile/edit-profile-page';
import { MemberProfilePage } from './lineoa/pages/MemberProfile/member-profile-page';
import { MemberSettingPage } from './lineoa/pages/MemberSetting/member-setting-page';
import { PrivacyAndPolicyPage } from './lineoa/pages/PrivacyAndPolicy/privacy-and-policy-page';
import { RewardPage } from './lineoa/pages/Rewards/rewards-page';
import { RewardsDetailPage } from './lineoa/pages/RewardsDetail/rewards-detail-page';
import { TermsAndConditionsPage } from './lineoa/pages/TermsAndConditions/terms-and-conditions-page';
import { TransactionPointPage } from './lineoa/pages/TransactionPoint/transaction-point-page';
import { NavigationWebsite } from './lineoa/components/navigationWebsite';
import { ReceiptPage } from './lineoa/pages/Receipt/receipt-page';
import { Global } from './global';

type MyFixtures = {
  //LineOA Pages
  signupPage: SigupPage;
  memberPrivilegePage: MemberPrivilegePage;
  oobePage: OobePage;
  contactUsPage: ContactUsPage;
  couponPrivilegePage: CouponsPrivilegePage;
  couponPrivilegeDetailPage: CouponsPrivilegeDetailPage;
  dataConsentPage: DataConsentPage;
  editProfilePage: EditProfilePage;
  memberProfilePage: MemberProfilePage;
  memberSettingPage: MemberSettingPage;
  privacyAndPolicyPage: PrivacyAndPolicyPage;
  rewardPage: RewardPage;
  rewardDetailPage: RewardsDetailPage;
  termAndConditionsPage: TermsAndConditionsPage;
  transactionPointPage: TransactionPointPage;
  receiptPage: ReceiptPage;
  global: Global

  //component
  navigationWebsite: NavigationWebsite;
};

export const test = base.extend<MyFixtures>({
  //LineOA Page
  signupPage: async ({ page }, use) => {
    const login = new SigupPage(page);
    await use(login);
  },
  memberPrivilegePage: async ({ page }, use) => {
    const memberPrivilegePage = new MemberPrivilegePage(page);
    await use(memberPrivilegePage);
  },
  oobePage: async ({ page }, use) => {
    const oobePage = new OobePage(page);
    await use(oobePage);
  },
  contactUsPage: async ({ page }, use) => {
    const contactUsPage = new ContactUsPage(page);
    await use(contactUsPage);
  },
  couponPrivilegePage: async ({ page }, use) => {
    const couponPrivilegePage = new CouponsPrivilegePage(page);
    await use(couponPrivilegePage);
  },
  couponPrivilegeDetailPage: async ({ page }, use) => {
    const couponPrivilegeDetailPage = new CouponsPrivilegeDetailPage(page);
    await use(couponPrivilegeDetailPage);
  },
  dataConsentPage: async ({ page }, use) => {
    const dataConsentPage = new DataConsentPage(page);
    await use(dataConsentPage);
  },
  editProfilePage: async ({ page }, use) => {
    const editProfilePage = new EditProfilePage(page);
    await use(editProfilePage);
  },
  memberProfilePage: async ({ page }, use) => {
    const memberProfilePage = new MemberProfilePage(page);
    await use(memberProfilePage);
  },
  memberSettingPage: async ({ page }, use) => {
    const memberSettingPage = new MemberSettingPage(page);
    await use(memberSettingPage);
  },
  privacyAndPolicyPage: async ({ page }, use) => {
    const privacyAndProlicyPage = new PrivacyAndPolicyPage(page);
    await use(privacyAndProlicyPage);
  },
  rewardDetailPage: async ({ page }, use) => {
    const rewardDetailPage = new RewardsDetailPage(page);
    await use(rewardDetailPage);
  },
  rewardPage: async ({ page }, use) => {
    const rewardPage = new RewardPage(page);
    await use(rewardPage);
  },
  termAndConditionsPage: async ({ page }, use) => {
    const termAndConditionsPage = new TermsAndConditionsPage(page);
    await use(termAndConditionsPage);
  },
  transactionPointPage: async ({ page }, use) => {
    const transactionPointPage = new TransactionPointPage(page);
    await use(transactionPointPage);
  },
  receiptPage: async ({ page }, use) => {
    const receiptPage = new ReceiptPage(page);
    await use(receiptPage)
  },
  global: async ({ page }, use) => {
    const global = new Global(page);
    await use(global)
  },

  //component
  navigationWebsite: async ({ page }, use) => {
    const navigationWebsite = new NavigationWebsite(page);
    await use(navigationWebsite);
  },
});
