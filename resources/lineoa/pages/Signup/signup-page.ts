import { expect, Locator, Page } from '@playwright/test';

export class SigupPage {
  readonly page: Page;

  // Main Page
  readonly citizenIDField: Locator;
  readonly mobileNumberField: Locator;
  readonly checkboxPrivacyAndPolicy: Locator;
  readonly buttonNext: Locator;
  readonly termAndConditionLink: Locator;
  readonly privacyPolicyLink: Locator;

  // Signup Page
  readonly firstNameField: Locator;
  readonly lastNameField: Locator;
  readonly GenderField: Locator;
  readonly birthDateField: Locator;
  readonly birthMonthField: Locator;
  readonly birthYearField: Locator;
  readonly nationalityField: Locator;
  readonly emailField: Locator;
  readonly checkboxPromotionAndNews: Locator;

  // OTP Page
  readonly otpField : string;

  // Manage personal data Page
  readonly buttonAccept: Locator;
  readonly buttonReject: Locator;
  readonly buttonBackToEdit: Locator;
  readonly buttonConfirmReject: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.citizenIDField = page.locator('#txtCitizenID');
    this.mobileNumberField = page.locator('#txtMobileNumber');
    this.checkboxPrivacyAndPolicy = page.locator('#chkTncAndPrivacyPolicy');
    this.buttonNext = page.locator('#btnNext');
    this.termAndConditionLink = page.locator("#hplTermsAndConditions")
    this.privacyPolicyLink = page.locator("#hplPrivacyPolicy")

    // Signup Page
    this.firstNameField = page.locator('#txtFirstName');
    this.lastNameField = page.locator('#txtLastName');
    this.GenderField = page.locator('#ddlGender');
    this.birthDateField = page.locator('#ddlBirthDate');
    this.birthMonthField = page.locator('#ddlBirthMonth');
    this.birthYearField = page.locator('#ddlBirthYear');
    this.nationalityField = page.locator('#ddlNationality');
    this.emailField = page.locator('#txtEmail');
    this.checkboxPromotionAndNews = page.locator('#chkPromotionsAndNews');

    // OTP Page
    this.otpField = 'input[data-id="{{number}}"]'

    // Manage personal data Page
    this.buttonAccept = page.locator("#btnAccept")
    this.buttonReject = page.locator("#btnNotAccept")
    this.buttonBackToEdit = page.locator('.btn.btn_primary.w_100')
    this.buttonConfirmReject = page.locator('.btn.btn_secondary.w_100')
  }

  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }

  async selectBirthDate(date: string) {
    await this.birthDateField.selectOption({ label: date });
  }

  async selectBirthMonth(month: string) {
    await this.birthMonthField.selectOption({ label: month });
  }

  async selectBirthYear(year: string) {
    await this.birthYearField.selectOption({ label: year });
  }

  async selectGender(gender: 'ชาย' | 'หญิง' | 'ไม่ระบุ') {
    await this.GenderField.selectOption({ label: gender });
  }

  async selectNationality(nationality: string) {
    await this.nationalityField.selectOption({ label: nationality });
  }

  async inputOTP(number : string){
    for (let index = 0; index < number.length; index++) {
        const sigleNumber = number[index];
        const newLocator = this.otpField.replace('{{number}}', index.toString())
        await this.page.waitForTimeout(500)
        await this.page.locator(newLocator).fill(sigleNumber);
    }
  }

  async signUpMainSection(locationSignup: string, locationTnc: string, locationPrivacyPolicy: string, citizenId: string, phone: string, checkAllLink = false) {
    await this.page.waitForURL(new RegExp(`/${locationSignup}/`))
    this.verifyUrl(locationSignup)
    await expect.soft(this.buttonNext).toBeDisabled()
    if (checkAllLink) {
      await this.termAndConditionLink.click()
      this.verifyUrl(locationTnc)
      this.page.goBack()

      await this.privacyPolicyLink.click()
      this.verifyUrl(locationPrivacyPolicy)
      this.page.goBack()
    }
    await this.citizenIDField.fill(citizenId)
    await this.mobileNumberField.fill(phone)
    await this.checkboxPrivacyAndPolicy.check()
    await expect.soft(this.buttonNext).toBeEnabled()
    await this.buttonNext.click()
  }

  async signUpPersonalDetailSection(locationPersonalDetail: string, personalDetail: {
      firstName: string,
      lastName: string,
      gender: 'ชาย' | 'หญิง' | 'ไม่ระบุ',
      dateOfBirth: string,
      monthOfBirth: string,
      yearOfBirth: string,
      yearOfBirthUnder10year: string,
      nationality: string,
      email: string
  }) {
    await this.page.waitForURL(new RegExp(`/${locationPersonalDetail}/`))
    await this.firstNameField.fill(personalDetail.firstName)
    await this.lastNameField.fill(personalDetail.lastName)
    await this.selectGender(personalDetail.gender)
    await this.selectBirthDate(personalDetail.dateOfBirth)
    await this.selectBirthMonth(personalDetail.monthOfBirth)
    await this.selectBirthYear(personalDetail.yearOfBirth)
    await this.selectNationality(personalDetail.nationality)
    await this.emailField.fill(personalDetail.email)
    await this.checkboxPromotionAndNews.check()
    await this.buttonNext.click()
  }

  async signUpOtpSection(locationOtp: string, otp: string) {
    await this.page.waitForURL(new RegExp(`/${locationOtp}/`))
    await this.inputOTP(otp)
  }

  async acceptAskToManagePersonalDataSection(locationAskToManagePersonalData: string) {
    await this.page.waitForURL(new RegExp(`/${locationAskToManagePersonalData}`))
    await this.buttonReject.click()
    await expect.soft(this.buttonBackToEdit).toBeVisible()
    await expect.soft(this.buttonConfirmReject).toBeVisible()
    await this.buttonConfirmReject.click()
    await expect.soft(this.buttonBackToEdit).not.toBeVisible()
    await expect.soft(this.buttonConfirmReject).not.toBeVisible()
  }
}
