import { expect, Locator, Page } from '@playwright/test';

export class CouponsPrivilegeDetailPage {
  readonly page: Page;

  // Main Page
  readonly buttonGenerateBarcode: Locator;
  readonly buttonGotoMyCoupon : Locator
  readonly buttonEmployeePressesToConfirmTheUseOfRights : Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonGenerateBarcode = page.locator('#btnGenerateBarcode');
    this.buttonGotoMyCoupon = page.getByText('คูปองของฉัน');
    this.buttonEmployeePressesToConfirmTheUseOfRights = page.getByText('พนักงานกดยืนยันการใช้สิทธิ์');
    this.buttonGenerateBarcode = page.locator('#btnGenerateBarcode');
    this.buttonGenerateBarcode = page.locator('#btnGenerateBarcode');
    this.buttonGenerateBarcode = page.locator('#btnGenerateBarcode');
  }
}
