import { expect, Locator, Page } from '@playwright/test';

export class ContactUsPage {
  readonly page: Page;

  // Main Page
  readonly buttonCall: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonCall = page.locator('#btnCall');
  }
}
