import { expect, Locator, Page } from '@playwright/test';

export class MemberProfilePage {
  readonly page: Page;

  // Main Page
  readonly buttonFlipCard: Locator;
  readonly buttonBarcode: Locator;
  readonly buttonQrcode: Locator;
  readonly buttonPassword: Locator;
  readonly buttonHistoryUsedPoint: Locator;
  readonly buttonGotoCouponPage: Locator;
  readonly buttonGotoRewardsPage: Locator;
  readonly buttonGotoReceiptsPage: Locator;
  readonly buttonGotoProfile: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonFlipCard = page.locator('#btnFlipCard');
    this.buttonBarcode = page.locator("li:text('บาร์โค้ด')");
    this.buttonQrcode = page.locator("li:text('คิวอาร์')");
    this.buttonPassword = page.locator("li:text('รหัส')");
    this.buttonHistoryUsedPoint = page.getByText('ดูประวัติการใช้แต้ม');
    this.buttonGotoCouponPage = page.locator('#gotoCouponPage');
    this.buttonGotoRewardsPage = page.locator('#gotoRewardsPage');
    this.buttonGotoReceiptsPage = page.locator('#gotoReceiptsPage');
    this.buttonGotoProfile = page.getByText('บัญชีของฉัน');
  }
  
  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }

  async verifyProfilePage(locationProfilePage: string) {
    await this.page.waitForURL(new RegExp(`/${locationProfilePage}/`))
    this.verifyUrl(locationProfilePage)
  }

  async navigateToMemberSetting(locationMemberSettingPage: string) {
    this.buttonGotoProfile.click()
    await this.page.waitForURL(new RegExp(`/${locationMemberSettingPage}/`))
    this.verifyUrl(locationMemberSettingPage)
  }
}
