import { expect, Locator, Page } from '@playwright/test';

export class OobePage {
  readonly page: Page;

  // Main Page
  readonly buttonNavigateToMemberProfile: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonNavigateToMemberProfile = page.locator('#btnNavigateToMemberProfile');
  }
  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }
  async clickToMemberProfile(locationOobe: string) {
    await this.page.waitForURL(new RegExp(`/${locationOobe}/`))
    this.verifyUrl(locationOobe)
    this.buttonNavigateToMemberProfile.click()
  }
}
