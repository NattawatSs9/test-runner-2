import { expect, Locator, Page } from '@playwright/test';

export class EditProfilePage {
  readonly page: Page;

  // Main Page
  readonly maritalStatusField: Locator;
  readonly occupationField: Locator;
  readonly occupationDescriptionField: Locator;
  readonly incomeField: Locator;
  readonly emailField: Locator;
  readonly buttonSave: Locator;
  readonly buttonCancel: Locator;
  readonly buttonContinueEditProfile: Locator;
  readonly buttonCancelEditProfile: Locator;
  readonly labelAskToLeave: Locator;
  readonly labelSaveSuccessfully: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.maritalStatusField = page.locator('#ddlMaritalStatus');
    this.occupationField = page.locator('#ddlOccupation');
    this.incomeField = page.locator('#ddlIncome');
    this.occupationDescriptionField = page.locator("#txtOccupationDescription")
    this.emailField = page.locator('#txtEmail');
    this.buttonSave = page.locator('#btnSave');
    this.buttonCancel = page.locator('#btnCancel');
    this.buttonContinueEditProfile = page.getByText('ดำเนินการแก้ไขข้อมูลต่อ');
    this.buttonCancelEditProfile = page.getByText('ยกเลิกการแก้ไขข้อมูล');
    this.labelAskToLeave = page.getByText('คุณต้องการออกจากหน้านี้หรือไม่')
    this.labelSaveSuccessfully = page.getByText('บันทึกสำเร็จ')
  }

  async selectMaritalStatus(status: string) {
    await this.maritalStatusField.selectOption({ label: status });
  }

  async selectOccupation(occupation: string) {
    await this.occupationField.selectOption({ label: occupation });
  }

  async selectIncome(income: string) {
    await this.incomeField.selectOption({ label: income });
  }

  async verifyTextInPage(text: string) {
    const element = this.page.getByText(text).first()
    expect(element).toBeVisible;
  }
}
