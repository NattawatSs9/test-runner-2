import { expect, Locator, Page } from '@playwright/test';

export class TransactionPointPage {
  readonly page: Page;

  // Main Page
  readonly pointExpired: Locator;
  readonly earnPoints: Locator;
  readonly exchangePoints: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.pointExpired = page.getByText('แต้มหมดอายุ');
    this.earnPoints = page.getByText('ได้รับแต้ม');
    this.earnPoints = page.getByText('แลกแต้ม');
  }
}
