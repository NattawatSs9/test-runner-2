import { expect, Locator, Page } from '@playwright/test';

export class MemberPrivilegePage {
  readonly page: Page;

  // Main Page
  readonly buttonBecomeTheMember: Locator;
  readonly labelRegisterFree: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonBecomeTheMember = page.locator('#btnBecomeTheMember');
    this.labelRegisterFree = page.getByText('สมัครฟรี!');
  }

  verifyUrl(verifyUrl: string) {
    const url = this.page.url()
    expect.soft(url).toContain(verifyUrl)
  }
  async clickBecomeTheMember(verifyMemberPrivilegeUrl: string) {
    await Promise.all([
      this.page.waitForResponse(resp => {
        return resp.url().includes('')
      })
    ]);
    this.verifyUrl(verifyMemberPrivilegeUrl)
    await expect.soft(this.buttonBecomeTheMember).toBeVisible()
    await this.buttonBecomeTheMember.click()
  }
}