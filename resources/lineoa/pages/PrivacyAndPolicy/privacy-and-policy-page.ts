import { expect, Locator, Page } from '@playwright/test';

export class PrivacyAndPolicyPage {
  readonly page: Page;

  // Main Page
  readonly textPolicyAndPrivacyHeader: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.textPolicyAndPrivacyHeader = page.locator("h3:text('นโยบายความเป็นส่วนตัว')");
  }
}
