import { expect, Locator, Page } from '@playwright/test';

export class CouponsPrivilegePage {
  readonly page: Page;

  // Main Page
  readonly buttonCouponCJ: Locator;
  readonly buttonCouponPartner: Locator;
  readonly buttonCouponDiscountProduct : Locator
  readonly buttonDiscountCash : Locator

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonCouponCJ = page.getByText('คูปองซีเจ');
    this.buttonCouponPartner = page.getByText('คูปองพาร์ทเนอร์');
    this.buttonCouponDiscountProduct = page.getByText('คูปองส่วนลดสินค้า');
    this.buttonDiscountCash = page.getByText('คูปองส่วนลดเงินสด');
  }

  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }

  async verifyCouponPrivilege(locationCouponPrivilege: string) {
    await this.page.waitForURL(new RegExp(`/${locationCouponPrivilege}/`))
    this.verifyUrl(locationCouponPrivilege)
  }

  async getDataFromSessionStorage() {
    const featureToggle = await this.page.evaluate(() => {
      const data = sessionStorage.getItem("featureToggle");
      return data ? JSON.parse(data) : null;
    });
    return featureToggle
  }

  async checkConditionTax(featureToggle: any, locationCouponPrivilege: string, searchPrivilegePocket: string, searchPocket: string) {
    if (featureToggle && featureToggle.PointRedemption) {
      const url = this.page.url();
      const search = new URL(url).search;
      
      this.verifyCouponPrivilege(locationCouponPrivilege)
      expect.soft(search).toContain(searchPrivilegePocket);
    } else {
      const url = this.page.url();
      const search = new URL(url).search;
      
      this.verifyCouponPrivilege(locationCouponPrivilege)
      expect.soft(search).toContain(searchPocket);
    }
  }
}
