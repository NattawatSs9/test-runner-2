import { expect, Locator, Page } from '@playwright/test';

export class RewardsDetailPage {
  readonly page: Page;

  // Main Page
  readonly buttonConfirmExchangePoints: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonConfirmExchangePoints = page.getByText('ยืนยันการแลกแต้ม');
  }
}
