import { expect, Locator, Page } from '@playwright/test';

export class DataConsentPage {
  readonly page: Page;

  // Main Page
  readonly checkboxAcceptCommu: Locator;
  readonly textDataConsentHeader: Locator;
  readonly buttonReadMore: Locator;
  readonly askToAcceptModalHeader: Locator;
  readonly buttonAccept: Locator;
  readonly buttonCancel: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.checkboxAcceptCommu = page.locator('#chkAcceptCommu');
    this.textDataConsentHeader = page.locator(
      "h3:text('การจัดการข้อมูลส่วนบุคคล')"
    );
    this.buttonReadMore = page.locator('#hplPrivacyPolicy');
    this.askToAcceptModalHeader = page.getByText('ยืนยันการขอถอนความยินยอม')
    this.buttonAccept = page.getByText('ยืนยัน').first()
    this.buttonCancel = page.getByText('ยกเลิก').first()
  }
}
