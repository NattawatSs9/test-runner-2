import { expect, Locator, Page } from '@playwright/test';

export class TermsAndConditionsPage {
  readonly page: Page;

  // Main Page
  readonly textTermsAndConditionsHeader: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.textTermsAndConditionsHeader = page.locator("h3:text('ข้อกำหนดและเงื่อนไข')");
  }
}
