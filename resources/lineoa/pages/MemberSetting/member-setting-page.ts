import { expect, Locator, Page } from '@playwright/test';

export class MemberSettingPage {
  readonly page: Page;

  // Main Page
  readonly buttonEditProfile: Locator;
  readonly buttonMyCoupon: Locator;
  readonly buttonETax: Locator;
  readonly buttonTax: Locator;
  readonly buttonHistoryUsedPoints: Locator;
  readonly buttonContactUS: Locator;
  readonly buttonTermsAndConditions: Locator;
  readonly buttonPrivacyAndolicy: Locator;
  readonly buttonPersonalInformationManagement: Locator;
  readonly buttonDisconnectAccount: Locator;
  readonly buttonThaiLang: Locator;
  readonly buttonEngLang: Locator;
  readonly buttonApprove: Locator;
  readonly buttonCancel: Locator;
  
  // Unlink account
  readonly askToUnlinkContent: Locator;

  constructor(page: Page) {
    this.page = page;

    // Main Page
    this.buttonEditProfile = page.getByText('แก้ไขข้อมูลส่วนตัว');
    this.buttonMyCoupon = page.getByText('คูปองของฉัน');
    this.buttonETax = page.getByText('ใบเสร็จ (E-Tax)');
    this.buttonTax = page.getByText('ใบเสร็จ');
    this.buttonHistoryUsedPoints = page.getByText('ดูประวัติการใช้แต้ม');
    this.buttonContactUS = page.getByText('ติดต่อเรา');
    this.buttonTermsAndConditions = page.getByText('ข้อกำหนดและเงื่อนไข');
    this.buttonPrivacyAndolicy = page.getByText('นโยบายความเป็นส่วนตัว');
    this.buttonPersonalInformationManagement = page.getByText(
      'การจัดการข้อมูลส่วนบุคคล'
    );
    this.buttonDisconnectAccount = page.locator('#hplUnlinkLine');
    this.buttonEngLang = page.locator('#tglLangEN');
    this.buttonThaiLang = page.locator('#tglLangTH');
    this.buttonApprove = page.locator('.btn.btn_primary.w_100').getByText('ยืนยัน');
    this.buttonCancel = page.locator('.btn.btn_primary.w_100').getByText('ยกเลิก');

    // Unlink Account
    this.askToUnlinkContent = page.getByText('หากต้องการเข้าระบบในครั้งถัดไป')
  }

  async getDataFromSessionStorage() {
    const featureToggle = await this.page.evaluate(() => {
      const data = sessionStorage.getItem("featureToggle");
      return data ? JSON.parse(data) : null;
    });
    return featureToggle
  }

  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }

  async verifyPage(locationPage: string) {
    await this.page.waitForURL(new RegExp(`/${locationPage}/`))
    this.verifyUrl(locationPage)
  }

  async verifyTextInPage(text: string) {
    const element = this.page.getByText(text).first()
    expect(element).toBeVisible;
  }

  async checkConditionTax(featureToggle: any, locationReceipt: string, locationMemberSettingPage: string) {
    if (featureToggle && featureToggle.Receipt) {
      if (featureToggle.ETax) {
        await expect.soft(this.buttonETax).toBeVisible()
        await this.buttonETax.click()
        await this.verifyPage(locationReceipt)
        await this.page.goBack()
        await this.verifyPage(locationMemberSettingPage)
      }
      else {
        await expect.soft(this.buttonTax).toBeVisible()
        await this.buttonTax.click()
        await this.verifyPage(locationReceipt)
        await this.page.goBack()
        await this.verifyPage(locationMemberSettingPage)
      }
    }
    else {
      await expect.soft(this.buttonETax).not.toBeVisible()
    }
  }
}
