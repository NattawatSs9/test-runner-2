import { expect, Locator, Page } from '@playwright/test';
import { NavType } from '../../../models/nav-model';

export class NavigationWebsite {
  readonly page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  async navigateTo(path: string, params = []) {
    const currentURL = process.env.DEFAULT_URL;
    const newURL = new URL(path, currentURL);
    const queryParams = new URLSearchParams();
    queryParams.set('utm_source', 'e2e');
    if(params.length != 0){
      for (const obj of params) {
        for (const [key, value] of Object.entries(obj)) {
          queryParams.set(key.toString(), value.toString());
        }
      }
    }
    newURL.search = queryParams.toString();
    await this.page.goto(newURL.toString());
    await this.page.waitForLoadState('networkidle', { timeout: 10000 });
  }

  async GoToWebsite(pageName: NavType = null, params = []) {
    await this.page.setViewportSize({ width: 480, height: 800 });
    await this.navigateTo('/');
    switch (pageName) {
      case 'MemberPrivilege':
        await this.navigateTo('/member-privilege/');
        break;
      case 'MemberProfile':
        await this.navigateTo('/member-profile/');
        break;
      case 'KidsClub':
        await this.navigateTo('/', params);
        break;
      case 'NineBeauty':
        await this.navigateTo('/', params);
        break;
      case 'Setting':
        await this.navigateTo('/member-setting/');
        break;
      case 'UpdateProfile':
        await this.navigateTo('/member-setting/update-profile/');
        break;
      case 'MyCoupon':
        await this.navigateTo('/coupons-privilege/?privilege=1&pocket=2');
        break;
      case 'Coupon':
        await this.navigateTo('/coupons/', params);
        break;
      case 'Mission':
        await this.navigateTo('/', params);
        break;
      case 'Signup':
        await this.navigateTo('/signup/');
        break;
      case 'Reward':
        await this.navigateTo('/rewards/');
        break;
      case 'RewardDetail':
        await this.navigateTo('/rewards/', params);
        break;
      case 'Receipts':
        await this.navigateTo(`/receipts/`);
        break;
      case 'TransactionPoint':
        await this.navigateTo(`/transaction-point/`);
        break;
      case 'ContactUs':
        await this.navigateTo(`/member-setting/contact-us/`);
        break;
      case 'TermsAndConditions':
        await this.navigateTo(`/member-setting/terms-and-conditions/`);
        break;
      case 'PrivacyAndPolicy':
        await this.navigateTo(`/member-setting/privacy-policy/`);
        break;
      case 'DataConsent':
        await this.navigateTo(`/member-setting/data-consent/`);
        break;
      default:
        await this.navigateTo('/');
        break;
    }
  }


  async GoToWebsiteWithParams(params = []) {
    await this.page.setViewportSize({ width: 480, height: 800 });
    await this.navigateToParams(params)
  }

  async navigateToParams(params = []) {
    const currentURL = process.env.DEFAULT_URL;
    const newURL = new URL(currentURL);
    const queryParams = new URLSearchParams();
    queryParams.set('utm_source', 'e2e');
    if(params.length != 0){
      for (const obj of params) {
        for (const [key, value] of Object.entries(obj)) {
          queryParams.set(key.toString(), value.toString());
        }
      }
    }
    newURL.search = queryParams.toString();
    await this.page.goto(newURL.toString());
    await this.page.waitForLoadState('networkidle', { timeout: 10000 });
  }
}
