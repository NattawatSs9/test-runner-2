import { Page, expect, Locator } from "@playwright/test";

export class Global {
  page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  verifyUrl(link: string) {
    const url = this.page.url()
    expect.soft(url).toContain(link)
  }

  async verifyPage(locationCouponPrivilege: string) {
    await this.page.waitForURL(new RegExp(`/${locationCouponPrivilege}/`))
    this.verifyUrl(locationCouponPrivilege)
  }
  
}
