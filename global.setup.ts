import type { FullConfig } from '@playwright/test';

async function globalSetup(config: FullConfig) {
  // SQL database
  process.env.DB_PWD = 'P@ssw0rd';
  process.env.DB_NAME_TOTE = 'cjlineoa_dev';
  process.env.HOST = '172.31.3.65';
  process.env.DB_USER = 'doppio';
  process.env.PORT = '5432';


  process.env.DEFAULT_URL = 'https://signac-app-dev.cjexpress.co.th/';
}

export default globalSetup;
