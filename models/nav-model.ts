export type NavType =
  | 'MemberPrivilege'
  | 'MemberProfile'
  | 'Setting'
  | 'UpdateProfile'
  | 'MyCoupon'
  | 'Signup'
  | 'Reward'
  | 'RewardDetail'
  | 'TransactionPoint'
  | 'ContactUs'
  | 'TermsAndConditions'
  | 'PrivacyAndPolicy'
  | 'DataConsent'
  | 'Receipts'
  | 'KidsClub'
  | 'NineBeauty'
  | 'Coupon'
  | 'Mission'

