const clearAllLocaltorage = async (page) => {
  await page.evaluate(() => {
    localStorage.clear();
  });
};

const clearAllSessionStorage = async (page) => {
  await page.evaluate(() => {
    sessionStorage.clear();
  });
};

const clearAllCookie = async (page) => {
  await page.context().clearCookies();
};

export const clearAllStorageAndCookie = async (browser) => {
  const context = await browser.newContext({
    acceptDownloads: true,
    ignoreHTTPSErrors: true,
  });
  const page = await context.newPage();
  await page.goto('/');
  await clearAllLocaltorage(page);
  await clearAllSessionStorage(page);
  await clearAllCookie(page);
  await page.close();
};
