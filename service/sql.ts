import { Client } from 'pg'
const client = new Client({
  user: process.env.DB_USER,
  host: process.env.HOST,
  database: process.env.DB_NAME_TOTE,
  password: process.env.DB_PWD,
  port: process.env.PORT,
})
client.connect()
export const clearDatabaseWithName = async (tableName: string) => {
  const sqlQuery = `TRUNCATE ${tableName} RESTART IDENTITY CASCADE`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.error('SQL error', error);
  }
};

export const createUnLinkedLineUser = async (memberNo = 'M0001842721') => {
  const sqlQuery = `INSERT INTO customers (created_at, updated_at, deleted_at, line_id, member_no, oa_status, register_status, "language", active)
		VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'signac_mock_line_id', '${memberNo}', 'UNFOLLOW', NULL, 'th', false)`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createLinkedLineUser = async (memberNo = 'M0001842721') => {
  const sqlQuery = `INSERT INTO customers (created_at, updated_at, deleted_at, line_id, member_no, oa_status, register_status, "language", active)
    VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'signac_mock_line_id', '${memberNo}', 'FOLLOW', 'S00001', 'th', true)`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createUnfollowLineUser = async () => {
  const sqlQuery = `INSERT INTO customers (created_at, updated_at, line_id, oa_status)
    VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'signac_mock_line_id', 'UNFOLLOW')`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createCampaign = async (type = 'PUBLIC') => {
  const sqlQuery = `INSERT INTO campaign_masters (created_at, updated_at, deleted_at, campaign_name, picture, coupon_id, effective_from, effective_to, status, type, audience_name, total_lead, total_coupon_limit, total_coupon_collected, campaign_uuid, pocket_id)
  VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'คูปองส่วนลด15บาท(น้ำตาลมิตรผลครบ 600บาท)', 'campaign_picture.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fc', 1)`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const updateCampaignLimit = async (memberNo = 'M0001842721') => {
  const sqlQuery = `UPDATE campaign_leads SET updated_at = CURRENT_TIMESTAMP, member_campaign_status = 'AVAILABLE', m_coupon_id = NULL WHERE
  campaign_master_id = 1 AND member_no = '${memberNo}'`;
  const sqlQuery2 = `UPDATE campaign_masters SET updated_at = CURRENT_TIMESTAMP, total_coupon_limit = 1 WHERE id = 1`;
  try {
    await client.query(sqlQuery);
    await client.query(sqlQuery2);
  } catch (error) {
    console.log(error);
  }
};

export const createPockets = async () => {
  const sqlQuery = `INSERT INTO coupon_pockets (id, created_at, updated_at, pocket_name_th, pocket_name_en)
  VALUES (1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'คูปองส่วนลดเงินสด','คูปองส่วนลดเงินสด'),
  (2, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'คูปองส่วนลดสินค้า','คูปองส่วนลดสินค้า')`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createEtaxMemberAddresses = async () => {
  const sqlQuery = `INSERT INTO public.etax_member_addresses
  (member_no, tax_invoice_type, house_no, postcode, email, created_at, updated_at, deleted_at, firstname, lastname, sub_district_id, district_id, province_id, tax_no, branch_no)
  VALUES('M0001842721', 'NIDN', '11/11', '30310', '123@email.com', '2023-11-21 17:19:42.535', '2023-11-21 17:19:42.535', NULL, 'ชื่อจริง', 'นามสกุลจริง', 1211, 188, 54, '3100100100124', ''),
  ('M0001842721', 'TXID', '11/11', '30310', '123@email.com', '2023-11-21 17:20:47.532', '2023-11-21 17:20:47.532', NULL, 'ชื่อบริษัทจริง', '', 1211, 188, 54, '3100100100159', '00001')`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createCampaignLeads = async (memberNo = 'M0001842721') => {
  const sqlQuery = `INSERT INTO campaign_leads (created_at, updated_at, deleted_at, campaign_master_id, member_no, member_campaign_status, m_coupon_id)
  VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, '${memberNo}', 'AVAILABLE', NULL)`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createCampaignLeadsCollected = async (
  memberNo = 'M0001842721'
) => {
  const sqlQuery = `INSERT INTO campaign_leads (created_at, updated_at, deleted_at, campaign_master_id, member_no, member_campaign_status, m_coupon_id)
  VALUES
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 1, '${memberNo}', 'COLLECTED', '626b5bf080e292fb6bb0d681'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 2, '${memberNo}', 'COLLECTED', '626b5bf080e292fb6bb0d682'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 3, '${memberNo}', 'COLLECTED', '626b5bf080e292fb6bb0d683'),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 4, '${memberNo}', 'COLLECTED', '626b5bf080e292fb6bb0d684')`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};

export const createCampaigns = async (type = 'PUBLIC') => {
  const sqlQuery = `INSERT INTO campaign_masters (created_at, updated_at, deleted_at, campaign_name, picture, coupon_id, effective_from, effective_to, status, type, audience_name, total_lead, total_coupon_limit, total_coupon_collected, campaign_uuid,pocket_id)
  VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 20 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fa',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fb',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fi',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fj',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fk',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fl',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fm',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fn',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fo',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 25 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fp',2),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'คูปองส่วนลด20บาท(น้ำตาลมิตรผลครบ 600บาท)', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fc',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'คูปองส่วนลด30บาท(น้ำตาลมิตรผลครบ 600บาท)', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fd',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 20 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fe',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'ใช้ 100 ลด 20 บาท', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/129.png', '000000000000000001', '2020-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55ff',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'คูปองส่วนลด20บาท(น้ำตาลมิตรผลครบ 600บาท)', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/054.png', '000000000000000001', '2022-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fg',1),
  (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, 'คูปองส่วนลด30บาท(น้ำตาลมิตรผลครบ 600บาท)', 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/063.png', '000000000000000001', '2022-01-01 00:00:00.000 +0700', '2029-12-31 23:59:59.000 +0700', 'ACTIVE', '${type}', 'Exam Audience', 0, 500, 0, 'b65786fe-3570-11ed-9dd1-bec9687c55fh',1)`;
  try {
    await client.query(sqlQuery);
  } catch (error) {
    console.log(error);
  }
};
