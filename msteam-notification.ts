const axios = require('axios');
const path = require('path');
const fs = require('fs');
const { mergeFiles } = require('junit-report-merger');
const { XMLParser } = require('fast-xml-parser');

const outputPath = './results_all_logs';
const outputFile = path.join(
  __dirname,
  'results_all_logs',
  'junit-all-results.xml'
);
const inputFiles = ['./result_log/*.xml'];

//https://cjexpressgroup.webhook.office.com/webhookb2/83c843dc-9994-4d55-b5b5-acb428ef0424@d6597d3c-c60b-49b5-a9b1-c470cfef3f56/IncomingWebhook/42af06f44eb147b59955b1031705a6cd/e1da9242-df5d-413b-95f0-0b1bd5c0a8cc
const URL_WEBHOOK = process.env.MSTEAM_WEBHOOK_URL;

const MergeJunitReport = async () => {
  if (!fs.existsSync(outputPath)) fs.mkdirSync(outputPath);
  try {
    await mergeFiles(outputFile, inputFiles);
    console.log('Merged, check ./results_all_logs/junit-all-results.xml');
  } catch (err) {
    console.error(err);
  }
};

const GetTestResultFromReport = () => {
  const xml = fs.readFileSync(`${outputPath}/junit-all-results.xml`, {
    encoding: 'utf-8',
  });
  const parser = new XMLParser({
    ignoreAttributes: false,
  });
  let { testsuites } = parser.parse(xml);
  return {
    total: Number(testsuites['@_tests']),
    failures: Number(testsuites['@_failures']),
    errors: Number(testsuites['@_errors']),
    skipped: Number(testsuites['@_skipped']),
  };
};

const SendNotification = async () => {
  if (!URL_WEBHOOK) return;
  await MergeJunitReport();
  const RESULT_DATA = await GetTestResultFromReport();
  const { total, failures, errors, skipped } = RESULT_DATA;
  const DATA_TO_SEND = {
    type: 'message',
    attachments: [
      {
        contentType: 'application/vnd.microsoft.card.adaptive',
        contentUrl: null,
        content: {
          type: 'AdaptiveCard',
          body: [
            {
              type: 'TextBlock',
              text: `Report POS Admin - Total Test Case = ${total}`,
              size: 'Large',
              isSubtle: true,
              wrap: true,
              style: 'heading',
            },
            {
              type: 'ColumnSet',
              columns: [
                {
                  type: 'Column',
                  width: 'stretch',
                  items: [
                    {
                      type: 'TextBlock',
                      text: `Pass : ${total - (failures + errors + skipped)}`,
                      horizontalAlignment: 'Left',
                      color: 'Good',
                      weight: 'Bolder',
                      wrap: true,
                    },
                    {
                      type: 'TextBlock',
                      text: `Fail : ${failures}`,
                      horizontalAlignment: 'Left',
                      color: 'attention',
                      weight: 'Bolder',
                      spacing: 'None',
                      wrap: true,
                    },
                    {
                      type: 'TextBlock',
                      text: `Error : ${errors}`,
                      horizontalAlignment: 'Left',
                      color: 'Warning',
                      spacing: 'None',
                      wrap: true,
                    },
                    {
                      type: 'TextBlock',
                      text: `Skip : ${skipped}`,
                      horizontalAlignment: 'Left',
                      color: 'Accent',
                      weight: 'Bolder',
                      spacing: 'None',
                      wrap: true,
                    },
                  ],
                },
              ],
            },
            {
              type: 'ColumnSet',
              columns: [
                {
                  type: 'Column',
                  width: 'stretch',
                  items: [
                    {
                      type: 'TextBlock',
                      text: `See Report [Click](https://cjexpress.pages-gitlab.cjexpress.io/qa/pos/posadmin-test-automation/)`,
                      horizontalAlignment: 'Left',
                      spacing: 'None',
                      wrap: true,
                    },
                  ],
                },
              ],
            },
          ],
          $schema: 'http://adaptivecards.io/schemas/adaptive-card.json',
          version: '1.6',
        },
      },
    ],
  };
  await axios
    .post(URL_WEBHOOK, DATA_TO_SEND)
    .then((res) => console.log('Send notification success'))
    .catch((err) => console.error(err));
};

SendNotification();
