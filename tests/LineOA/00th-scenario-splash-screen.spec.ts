import { expect } from 'playwright/test';
import { test } from '../../resources/fixture';
import { clearAllStorageAndCookie } from '../../service/clearCache';
import {
  clearDatabaseWithName,
  createUnLinkedLineUser,
  createLinkedLineUser,
  createCampaign,
  createCampaignLeads,
  createPockets,
} from '../../service/sql';

import { testdata } from '../../data/testdata/testdata-00th.json';
let currentURL, urlSearchParams;

test.describe('00 : Unfollow user flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to error page with code e0200 when user unfollow', async ({
    page,
    navigationWebsite,
  }) => {
    await createUnLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([ { utm_source : 'e2e' } ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/error/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('code')).toBe('E0200');
  });
});

test.describe('00 : Register flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to member privilege page when unlinked user entrance via normal url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([ { utm_source : 'e2e' } ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });

  test('should navigate to member profile page when linked user entrance via normal url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([ { utm_source : 'e2e' } ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-profile/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });
});

test.describe('00 : Kids club flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to member privilege page when unlinked user entrance via kidsclub url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([{ channel: 'kidsclub' }, { utm_source : 'e2e' }]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });

  test('should navigate to prize catalog page when linked user entrance via kidsclub url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([{ channel: 'kidsclub' }, { utm_source : 'e2e' }]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect
      .soft(currentURL.split('?')[0])
      .toContain('/redemption/prize-catalog/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });
});

test.describe('00 : Nine beauty flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to member privilege page when unlinked user entrance via ninebeauty url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([{ channel: 'ninebeauty' }, { utm_source : 'e2e' }]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });

  test('should navigate to member profile page when linked user entrance via ninebeauty url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([{ channel: 'ninebeauty' }, { utm_source : 'e2e' }]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-profile/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });
});

test.describe('00 : Campaign flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
    await clearDatabaseWithName(testdata.db.campaign_leads);
    await clearDatabaseWithName(testdata.db.campaign_masters);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
    await clearDatabaseWithName(testdata.db.campaign_leads);
    await clearDatabaseWithName(testdata.db.campaign_masters);
  });

  test('should navigate to coupons page when unlinked user entrance via campaign url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([
      { subject: 'campaign' },
      { code: testdata.MockData.campaignCode.available },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect
      .soft(urlSearchParams.get('code'))
      .toBe(testdata.MockData.campaignCode.available);
  });

  test('should navigate to coupons page when linked user entrance via campaign url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await createCampaign(testdata.campaign.type.target);
    await createCampaignLeads();
    await navigationWebsite.GoToWebsiteWithParams([
      { subject: 'campaign' },
      { code: testdata.MockData.campaignCode.available },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect
      .soft(urlSearchParams.get('code'))
      .toBe(testdata.MockData.campaignCode.available);
  });
});

test.describe('00 : Mission flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to member privilege page with missionUUID in session storage when unlinked user entrance via mission url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([
      { subject: 'mission' },
      { code: testdata.MockData.missionCode.available },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect
      .soft(urlSearchParams.get('code'))
      .not.toBe(testdata.MockData.missionCode.available);

    await page.waitForLoadState('networkidle', { timeout: 10000 });
    const sessionStorageValue = await page.evaluate(() => {
      return sessionStorage.getItem('missionUUID');
    });

    expect(sessionStorageValue).toContain(
      testdata.MockData.missionCode.available
    );
  });

  test('should navigate to member profile page without missionUUID in session storage when linked user entrance via mission url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([
      { subject: 'mission' },
      { code: testdata.MockData.missionCode.available },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-profile/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect
      .soft(urlSearchParams.get('code'))
      .not.toBe(testdata.MockData.missionCode.available);
    await page.waitForLoadState('networkidle', { timeout: 10000 });
    const sessionStorageValue = await page.evaluate(() => {
      return sessionStorage.getItem('missionUUID');
    });
    expect(sessionStorageValue).toBe(null);
  });
});

test.describe('00 : Point redemption @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
    await clearDatabaseWithName(testdata.db.coupon_pockets);
    await createPockets();
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
    await clearDatabaseWithName(testdata.db.coupon_pockets);
  });

  test('should navigate to member privilege page when unlinked user entrance via point redemption url', async ({
    page,
    navigationWebsite,
  }) => {
    await navigationWebsite.GoToWebsiteWithParams([
      { privilege: '1' },
      { pocket: 1 },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/member-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe(null);
    expect.soft(urlSearchParams.get('pocket')).toBe(null);
  });

  test('should navigate to coupons privilege page when linked user entrance via point redemption url', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([
      { privilege: '1' },
      { pocket: '1' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe('1');
    expect.soft(urlSearchParams.get('pocket')).toBe('1');

    await navigationWebsite.GoToWebsiteWithParams([
      { privilege: '' },
      { pocket: '' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe('1');
    expect.soft(urlSearchParams.get('pocket')).toBe('2');
  });

  test('should navigate to coupons privilege page when linked user entrance via point redemption url (only 1 param)', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([
      { privilege: '1' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe('1');
    expect.soft(urlSearchParams.get('pocket')).toBe(null);

    await navigationWebsite.GoToWebsiteWithParams([
      { pocket: '1' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe(null);
    expect.soft(urlSearchParams.get('pocket')).toBe('1');

    await navigationWebsite.GoToWebsiteWithParams([
      { privilege: '' },
      { pocket: '' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/coupons-privilege/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
    expect.soft(urlSearchParams.get('privilege')).toBe('1');
    expect.soft(urlSearchParams.get('pocket')).toBe('2');
  });
});


test.describe('00 : Destination Param @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser)
    await clearDatabaseWithName(testdata.db.customers);
  });

  test('should navigate to rewards page when linked user entrance via normal url with destination param (dest=rwp)', async ({
    page,
    navigationWebsite,
  }) => {
    await createLinkedLineUser();
    await navigationWebsite.GoToWebsiteWithParams([
      { dest: 'rwp' },
    ]);
    currentURL = page.url();
    urlSearchParams = new URLSearchParams(currentURL.split('?')[1]);
    expect.soft(currentURL.split('?')[0]).toContain('/rewards/');
    expect.soft(urlSearchParams.get('utm_source')).toBe('e2e');
  });
});
