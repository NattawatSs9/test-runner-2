import { expect } from 'playwright/test';
import { test } from '../../resources/fixture';
import { clearAllStorageAndCookie } from '../../service/clearCache';
import {
  clearDatabaseWithName,
  createUnLinkedLineUser,
  createLinkedLineUser,
} from '../../service/sql';
import { testdata } from '../../data/testdata/testdata-02nd.json';

test.describe('02 : First Time Register LineOA For Existing Member Flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser);
    await clearDatabaseWithName(testdata.db.customerTable);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser);
    await clearDatabaseWithName(testdata.db.customerTable);
  });
  test('Able to complete register flow for existing register', async ({
    page,
    navigationWebsite,
    memberPrivilegePage,
    signupPage,
    oobePage,
    memberProfilePage,
  }) => {
    await navigationWebsite.navigateTo('/');
    await page.waitForLoadState('networkidle', { timeout: 10000 });
    await memberPrivilegePage.clickBecomeTheMember(
      testdata.expectedData.locationMemberPrivilege.pathname
    );
    await signupPage.signUpMainSection(
      testdata.expectedData.locationSignup.pathname,
      testdata.expectedData.locationTnC.pathname,
      testdata.expectedData.locationPrivacyPolicy.pathname,
      testdata.initialData.existingMember.cid,
      testdata.initialData.existingMember.mobileNumber
    );
    await signupPage.acceptAskToManagePersonalDataSection(
      testdata.expectedData.locationAskToManagePersonalData.pathname
    );
    await signupPage.signUpOtpSection(
      testdata.expectedData.locationOtp.pathname,
      testdata.initialData.mockOTP.otp
    );
    await oobePage.clickToMemberProfile(
      testdata.expectedData.locationOobe.pathname
    );
    await memberProfilePage.verifyProfilePage(
      testdata.expectedData.locationProfileMember.pathname
    );
  });
});
