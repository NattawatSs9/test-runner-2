import { expect } from 'playwright/test';
import { test } from '../../resources/fixture';
import { clearAllStorageAndCookie } from '../../service/clearCache';
import {
  clearDatabaseWithName,
  createCampaigns,
  createLinkedLineUser,
  createPockets,
} from '../../service/sql';
import { testdata } from '../../data/testdata/testdata-04th.json';


test.describe('04 : Member Setting Page @testFunctional', () => {
    test.beforeEach(async ({ browser }) => {
      await clearAllStorageAndCookie(browser);
      await clearDatabaseWithName(testdata.db.customerTable);
      await clearDatabaseWithName(testdata.db.couponPocketsTable);
      await createPockets()
      await createCampaigns("TARGET")
      await createLinkedLineUser()
    });

    test.afterEach(async ({ browser }) => {
      await clearAllStorageAndCookie(browser);
      await clearDatabaseWithName(testdata.db.customerTable);
      await clearDatabaseWithName(testdata.db.couponPocketsTable);
      await clearDatabaseWithName(testdata.db.campaignMasters)
    });
    test('Able to Navigate to Each Sub Menu', async ({
      page,
      navigationWebsite,
      memberProfilePage,
      memberSettingPage,
      editProfilePage,
      couponPrivilegePage,
      contactUsPage,
      global,
      termAndConditionsPage,
      privacyAndPolicyPage,
      dataConsentPage,
      memberPrivilegePage
    }) => {
      await navigationWebsite.navigateTo('/');
      await page.waitForLoadState('networkidle', { timeout: 10000 });

      await global.verifyPage(testdata.expectedData.locationProfileMember.pathname)
      await memberProfilePage.buttonGotoProfile.click()

      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)
      await memberSettingPage.verifyTextInPage(testdata.expectedData.text.myAccount.TH)
      await memberSettingPage.buttonEditProfile.click()

      await global.verifyPage(testdata.expectedData.locationUpdateProfilePage.pathname)
      await editProfilePage.verifyTextInPage(testdata.expectedData.text.editPersonalData)

      await editProfilePage.selectMaritalStatus(testdata.initialData.member.maritalStatus)
      await editProfilePage.selectOccupation(testdata.initialData.member.occupation)
      await editProfilePage.occupationDescriptionField.fill(testdata.initialData.member.occupationDescription)
      await editProfilePage.selectIncome(testdata.initialData.member.income)
      await editProfilePage.emailField.clear()
      await editProfilePage.emailField.fill(testdata.initialData.member.email)

      await page.goBack()

      await expect.soft(editProfilePage.labelAskToLeave).toBeVisible()
      await editProfilePage.buttonContinueEditProfile.click()
      await editProfilePage.buttonCancel.click()
      await expect.soft(editProfilePage.labelAskToLeave).toBeVisible()
      await editProfilePage.buttonContinueEditProfile.click()
      await editProfilePage.buttonSave.click()
      await expect.soft(editProfilePage.labelSaveSuccessfully).toBeVisible()

      await global.verifyPage(testdata.expectedData.locationProfileMember.pathname)
      await memberProfilePage.buttonGotoProfile.click()

      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)
      await memberSettingPage.verifyTextInPage(testdata.expectedData.text.myAccount.TH)

      // --- My coupon --- //
        
      let featureToggle = await couponPrivilegePage.getDataFromSessionStorage()
      await memberSettingPage.buttonMyCoupon.click()
      await global.verifyPage(testdata.expectedData.locationCouponPrivilege.pathname)
      await couponPrivilegePage.checkConditionTax(featureToggle, testdata.expectedData.locationCouponPrivilege.pathname, testdata.expectedData.search.privilegePocket, testdata.expectedData.search.pocket)
      await page.goBack()
      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)

      // --- Receipt --- //

      featureToggle = await memberSettingPage.getDataFromSessionStorage()
      await memberSettingPage.checkConditionTax(featureToggle, testdata.expectedData.locationReceipt.pathname, testdata.expectedData.locationMemberSettingPage.pathname)

      // --- Change language --- //
      
      await memberSettingPage.buttonEngLang.evaluate('element => element.checked=true')
      await memberSettingPage.verifyTextInPage(testdata.expectedData.text.myAccount.EN)
      await memberSettingPage.buttonThaiLang.evaluate('element => element.checked=true')
      await memberSettingPage.verifyTextInPage(testdata.expectedData.text.myAccount.TH)

      // --- Contact Us --- //

      await memberSettingPage.buttonContactUS.click()
      await global.verifyPage(testdata.expectedData.locationContactUs.pathname)
      await expect.soft(contactUsPage.buttonCall).toBeVisible()
      await page.goBack()
      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)


      // --- TnC --- //

      await memberSettingPage.buttonTermsAndConditions.click()
      await global.verifyPage(testdata.expectedData.locationTnC.pathname)
      await expect.soft(termAndConditionsPage.textTermsAndConditionsHeader).toBeVisible()
      await page.goBack()
      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)

      // --- Privacy Policy --- //

      await memberSettingPage.buttonPrivacyAndolicy.click()
      await global.verifyPage(testdata.expectedData.locationPrivacyPolicy.pathname)
      await expect.soft(privacyAndPolicyPage.textPolicyAndPrivacyHeader).toBeVisible()
      await page.goBack()
      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)

      // -- Data Consent --- //

      await memberSettingPage.buttonPersonalInformationManagement.click()
      await global.verifyPage(testdata.expectedData.locationDataConsent.pathname)
      await expect.soft(dataConsentPage.textDataConsentHeader).toBeVisible()
      await expect.soft(dataConsentPage.checkboxAcceptCommu).toBeChecked()
      await expect.soft(dataConsentPage.checkboxAcceptCommu).toBeEnabled()
      await dataConsentPage.checkboxAcceptCommu.click()
      await expect.soft(dataConsentPage.askToAcceptModalHeader).toBeVisible()
      await dataConsentPage.buttonCancel.click()
      await expect.soft(dataConsentPage.checkboxAcceptCommu).toBeChecked()
      await expect.soft(dataConsentPage.checkboxAcceptCommu).toBeEnabled()
      await dataConsentPage.checkboxAcceptCommu.click()
      await expect.soft(dataConsentPage.askToAcceptModalHeader).toBeVisible()
      await dataConsentPage.buttonAccept.click()
      await global.verifyPage(testdata.expectedData.locationDataConsent.pathname)
      await page.goBack()
      await global.verifyPage(testdata.expectedData.locationMemberSettingPage.pathname)

      // --- Unlink account --- //

      await expect.soft(memberSettingPage.buttonDisconnectAccount).toBeVisible()
      await memberSettingPage.buttonDisconnectAccount.click()
      await expect.soft(memberSettingPage.askToUnlinkContent).toBeVisible()
      await memberSettingPage.buttonCancel.click()
      await memberSettingPage.buttonDisconnectAccount.click()
      await expect.soft(memberSettingPage.askToUnlinkContent).toBeVisible()
      await memberSettingPage.buttonApprove.click()
      await global.verifyPage(testdata.expectedData.locationMemberPrivilege.pathname)

      // --- Member Privilege --- //

      await expect.soft(memberPrivilegePage.labelRegisterFree).toBeVisible()
    });
  });
  