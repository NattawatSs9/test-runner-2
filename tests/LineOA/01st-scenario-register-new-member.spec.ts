import { expect } from 'playwright/test';
import { test } from '../../resources/fixture';
import { clearAllStorageAndCookie } from '../../service/clearCache';
import {
  clearDatabaseWithName,
  createUnLinkedLineUser,
  createLinkedLineUser,
} from '../../service/sql';
import { testdata } from '../../data/testdata/testdata-01st.json';

test.describe('01 : First Time Register LineOA For New Member Flow @testFunctional', () => {
  test.beforeEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser);
    await clearDatabaseWithName(testdata.db.customerTable);
  });
  test.afterEach(async ({ browser }) => {
    await clearAllStorageAndCookie(browser);
    await clearDatabaseWithName(testdata.db.customerTable);
  });
  test('Able to complete register flow for new register', async ({
    page,
    navigationWebsite,
    memberPrivilegePage,
    signupPage,
    oobePage,
    memberProfilePage,
  }) => {
    await navigationWebsite.navigateTo('/');
    await page.waitForLoadState('networkidle', { timeout: 10000 });
    await memberPrivilegePage.clickBecomeTheMember(
      testdata.expectedData.locationMemberPrivilege.pathname
    );
    await signupPage.signUpMainSection(
      testdata.expectedData.locationSignup.pathname,
      testdata.expectedData.locationTnC.pathname,
      testdata.expectedData.locationPrivacyPolicy.pathname,
      testdata.initialData.newMember.cid,
      testdata.initialData.newMember.mobileNumber
    );
    await signupPage.signUpPersonalDetailSection(
      testdata.expectedData.locationPersonalDetail.path,
      {
        firstName: testdata.initialData.newMember.personalDetail.firstName,
        lastName: testdata.initialData.newMember.personalDetail.lastName,
        gender: 'ชาย',
        dateOfBirth: testdata.initialData.newMember.personalDetail.dateOfBirth,
        monthOfBirth:
          testdata.initialData.newMember.personalDetail.monthOfBirth,
        yearOfBirth: testdata.initialData.newMember.personalDetail.yearOfBirth,
        yearOfBirthUnder10year:
          testdata.initialData.newMember.personalDetail.yearOfBirthUnder10year,
        nationality: testdata.initialData.newMember.personalDetail.nationality,
        email: testdata.initialData.newMember.personalDetail.email,
      }
    );
    await signupPage.signUpOtpSection(
      testdata.expectedData.locationOtp.pathname,
      testdata.initialData.mockOTP.otp
    );
    await oobePage.clickToMemberProfile(
      testdata.expectedData.locationOobe.pathname
    );
    await memberProfilePage.verifyProfilePage(
      testdata.expectedData.locationProfileMember.pathname
    );
  });
});
