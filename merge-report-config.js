const { mergeHTMLReports } = require("playwright-merge-html-reports");
const { readdirSync } = require("fs");
const path = require("path");

const reportsFolder1 = "./html-reports-1";
const reportsFolder2 = "./html-reports-2";
const reportsFolder3 = "./html-reports-3";
const reportsFolder4 = "./html-reports-4";
const reportsFolder5 = "./html-reports-5";

const getDirectoriesPaths = (folder) =>
  readdirSync(folder, { withFileTypes: true })
    .filter((item) => item.isDirectory())
    .map(({ name }) => path.resolve(folder, name));

const config = {
  outputFolderName: "merged-html-report", // default value
  outputBasePath: process.cwd(), // default value
};

const directoriesPaths = [
  ...getDirectoriesPaths(reportsFolder1),
  ...getDirectoriesPaths(reportsFolder2),
  ...getDirectoriesPaths(reportsFolder3),
  ...getDirectoriesPaths(reportsFolder4),
  ...getDirectoriesPaths(reportsFolder5),
];
mergeHTMLReports(directoriesPaths, config);
