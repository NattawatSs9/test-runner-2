# posadmin-test-automation

## How to install

1. Install Nodejs [click](https://nodejs.org/en/download/)
1. Install Extension in VScode

- Prettier
- ESLint

2. Go to Preferances -> Setting
3. Type in Search Setting
   | Name | Action |
   |--|--|
   | Format On Save | Enable |
   | Editor: Default Formatter | Prettier - Code Formatter |
4. Open Terminal and Type

```
npm install
```

## How to start

```
npm run test
```
