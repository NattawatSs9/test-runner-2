from    os          import  path
from    datetime    import  datetime
import  os
import  json
import  time
import  sys
import  base64
import  requests
import os
from datetime import datetime
import zipfile

print("STARTING SENDER....")

jenkins_data = {
    "jenkins_build_url" : os.environ.get('BUILD_URL') if os.environ.get('BUILD_URL') else "",
    "jenkins_build_id" : os.environ.get('BUILD_NUMBER') if os.environ.get('BUILD_NUMBER') else "No build id",
    "jenkins_branch" : os.environ.get('BRANCH') if os.environ.get('BRANCH') else "",
    "jenkins_GIT_BRANCH" : os.environ['CI_COMMIT_REF_NAME'] if os.environ['CI_COMMIT_REF_NAME'] else "",
    "jenkins_project_id" : os.environ.get('PROJECT_ID') if os.environ.get('PROJECT_ID') else "94b9c21b-d1d7-46a8-a79b-08c00a7df8af",
    "jenkins_company_id" : os.environ.get('COMPANY_ID') if os.environ.get('COMPANY_ID') else "dc7002d2-0d27-4d24-8954-20aa7aa492da",
    "jenkins_sub_project_id" : os.eenviron.get('SUB_PROJECT_ID') if os.environ.get('SUB_COMPANY_ID') else "64da3967-0077-4532-bb96-2bc89ed60f08",
    "jenkins_automated_version" : os.environ.get('AUTOMATED_VERSION') if os.environ.get('AUTOMATED_VERSION') else 1,
    "jenkins_app_version" : os.environ.get('APP_VERSION') if os.environ.get('APP_VERSION') else 1,
}
type = os.environ.get('TYPE') if os.environ.get('TYPE') else "junit"
job = os.environ.get('JOB_NAME') if os.environ.get('JOB_NAME') else "job"
build_type = os.environ.get('BUILD_TYPE') if os.environ.get('BUILD_TYPE') else ""
report_path = "./result_log/results.xml"
screenshot_folder = "./result_log/results.xml"

with zipfile.ZipFile("result.zip", 'w', zipfile.ZIP_DEFLATED) as zip_file:
    if os.path.isfile(report_path):
        zip_file.write(report_path, os.path.basename(report_path))

def send_report(sub_project_id):
    file={'report': open('result.zip','rb',)}
    for (root,dirs,files) in os.walk(screenshot_folder, topdown=True):
        for filename in files:
            if filename.endswith(".png"):
                file_path = os.path.join(root,filename)
                with open(file_path, "rb") as image:
                    encoded_data = base64.b64encode(image.read()).decode('utf-8')
                jenkins_data[filename] = encoded_data
    jenkins_data["job"] = job
    jenkins_data["type"] = type
    jenkins_data["build_type"] = build_type
    jenkins_data["report_filename"] = 'results.xml'
    endpoint = "https://failure-declaration.doppio-tech.com/v1/companies/" + str(jenkins_data["jenkins_company_id"]) + "/projects/" + str(sub_project_id) + "/builds"
    resp = requests.post(
        endpoint,
        files=file,
        data=jenkins_data)
    print(resp.status_code)
    print(resp.text)
    print(endpoint)
    return resp

def send_slack_message(resp_text):
    resp_json = resp_text.json()
    if len(sys.argv) < 2:
        print("Usage: python my_script.py <my_variable>")
        sys.exit(1)
    webhook = sys.argv[1]
    build_id = sys.argv[2]
    if resp_json['fail_count'] == 0:
        prefix = ':white_check_mark:'
    else:
        prefix = ":bomb:"
    total_min = int((float(resp_json['duration_ms'])/1000/60)//1)
    total_second = int((float(resp_json['duration_ms'])/1000%60)//1)
    total_test_time = "After " + str(total_min) + " minute(s) " + str(total_second) + " second(s)"
    job_url = "<https://gitlab.cjexpress.io/cjexpress/qa/pos/posadmin-test-automation/-/jobs/" + build_id + ">"
    slack_message = prefix + " CJ Automation UI\n" + "PASS: " + str(resp_json['pass_count']) + "\nFAIL: " + str(resp_json['fail_count']) + "\n"
    return requests.post(webhook, json.dumps({"text": slack_message, "attachments": [{"fallback": "(Report)", "text": job_url, "color": "#3641a6"}]}), verify=False)

if __name__ == '__main__':
    if jenkins_data["jenkins_GIT_BRANCH"] == "dev":
        resp_text = send_report("82207d80-7691-404f-acbe-e8c2f7ac3d52")
    elif jenkins_data["jenkins_GIT_BRANCH"] == "doppio-milestone2":
        resp_text = send_report("64da3967-0077-4532-bb96-2bc89ed60f08")
        send_slack_message(resp_text)